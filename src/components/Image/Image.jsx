import React, { Component } from "react";
import "./Image.scss";

export default class Image extends Component {
  render() {
    return (
      <div className={this.props.showBorder ? "image" : "imageNoBorder"}>
        { this.props.showTitle ? <div className="image-title">{this.props.title}</div> : '' }
        <div className={this.props.showBorder ? "image-image" : "image-imageNoBorder"}>
          <img src={this.props.src} alt={this.props.alt} />
        </div>
      </div>
    );
  }
}
