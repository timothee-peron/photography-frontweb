import React from "react";
import { render } from '@testing-library/react';
import Image from ".";

describe("Testing Image component", () => {
  const url='http://example.com/img.jpg';
  const alt='an example image';

  it("Image has a title", () => {
      const wrapper = render(<Image title="Title" src={url} showTitle={true} showBorder={true} />);
      const title = wrapper.container.querySelector(".image-title");
      expect(title.innerHTML).to.contain("Title");
   });


  it("Image has no title", () => {
      const wrapper = render(<Image title="Title" src={url} showTitle={false} showBorder={true} />);
      const title = wrapper.container.querySelector(".image-title");
      expect(title).toBeNull();
  });

   it("Image has an img with valid src", () => {
      const image = render(<Image title="Title" src={url} showTitle={true} showBorder={true} />);
      const img = image.container.querySelector(".image-image");
      const src = img.querySelector("[src=\"" + url + "\"]");
      expect(src).to.exist;
   });

   it("Image has an img with no alt by default", () => {
      const image = render(<Image title="Title" src={url} showTitle={true} showBorder={true} />);
      const img = image.container.querySelector(".image-image");
      const src = img.querySelector("[src]");
      expect(src.innerHTML).to.not.contain("alt");
   });

   it("Image has an img with valid alt", () => {
      const image = render(<Image title="Title" src={url} alt={alt} showTitle={true} showBorder={true} />);
      const img = image.container.querySelector(".image-image");
      const src = img.querySelector("[alt=\"" + alt + "\"]");
      expect(src).to.exist;
   });
});
