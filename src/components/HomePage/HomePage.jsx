import React, { Component } from "react";
import axios from "axios";
import Hammer from 'hammerjs';
import "./homepage.scss";
import ContentHolder from '../ContentHolder';
import TabsMenu from '../ContentHolder/TabsMenu';

class HomePage extends Component {
  constructor() {
    super();
    this.state = {
      title: "",
      imgs: [],
      tabs: [],
      loading: true,
      index: 0,
    };

    this.ref = React.createRef()
    this.hammer = null;
  }

  addHammer() {
    const elem = this.ref && this.ref.current;
    if(!elem) return; // not properly mounted
    if(this.hammer) return; // already init
    this.hammer = new Hammer(elem);
    this.hammer.on("swiperight", this.tabPrevious.bind(this));
    this.hammer.on("swipeleft", this.tabNext.bind(this));
  }

  componentDidMount() {
    axios.get("/pictures.json").then(response => {
      this.setState({
        title: response.data.title,
        imgs: response.data.pictures,
        tabs: response.data.tabs,
        loading: false,
      });
    });

    this.addHammer();
  }

  componentDidUpdate(){
    this.addHammer();
  }

  getCurrentTabIndex(){
    return this.state.index;
  }

  isBlurred(){
    return this.props.blurred;
  }

  tabPrevious() {
    if (this.getCurrentTabIndex() > 0) {
      this.setState({index: this.getCurrentTabIndex() - 1});
    }
  }

  tabNext() {
    if (this.getCurrentTabIndex() < this.state.tabs.length - 1) {
      this.setState({index: this.getCurrentTabIndex() + 1});
    }
  }

  render() {
    if(this.state.loading){
      return '';
    }
    if(this.props.config.tabs){
      return (
        <div ref={this.ref} className={ (this.isBlurred() ? "homepage-blur" : "homepage") }>
          { this.props.config.title ? <h1>{this.state.title}</h1> : '' }
          <TabsMenu 
            index={this.getCurrentTabIndex()} // current tab index
            showPrevious={this.tabPrevious.bind(this)}
            showNext={this.tabNext.bind(this)}
            tabs={this.state.tabs} // array of tabs
            />
          <div id='contentSlider' style={{'transform': 'translateX(' + -100*this.getCurrentTabIndex() + '%)'}}>
            <div id='contentHolders' style={{'width': + 100*this.state.tabs.length + '%'}}>
              {this.state.tabs.map((tab, index, tabs) => 
                <ContentHolder
                    key={index}
                    config={this.props.config}
                    imgs={this.state.imgs.filter(a=>(a.tab ===tab))}
                    showSide={index - this.getCurrentTabIndex()}
                    />
              )}
            </div>
          </div>
        </div>
      );
    }
    else {
      return (
        <div ref={this.myRef} className={ (this.isBlurred() ? "homepage-blur" : "homepage") }>
          { this.props.config.title ? <h1>{this.state.title}</h1> : '' }
          <ContentHolder config={this.props.config} imgs={this.state.imgs} />
        </div>
      );
    }
  }
}

export default HomePage;
