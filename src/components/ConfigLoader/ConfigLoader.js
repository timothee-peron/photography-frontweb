import axios from "axios";
import defaultConfig from "./default_config.json";

class ConfigLoader{
  constructor() {
  }

  async load(){
    return axios.get("/config.json", {headers: {"Accept": "application/json"}})
      .then(response => {
        if( !response.headers['content-type'].includes("application/json")) {
          this.config=defaultConfig;
        }
        else {
          this.config=response.data;
        }
      })
      .catch(() /*error*/ => { // to test!
        // console.error('Could not load confif.json from server: ' + error);
        this.config=defaultConfig;
      });
  }

  getConfig() {
    return this.config;
  }

}


export default ConfigLoader;
