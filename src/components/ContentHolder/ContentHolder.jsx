import React, { Component } from "react";
import Image from '../Image';
import "./ContentHolder.scss";

// for the observer api call
const optionsObserver = {
  root: null,
  rootMargin: "0px",
  threshold: 0.5,
};

const loadPictureByPack = 1;
const initialPictures = 5;

export default class ContentHolder extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      lastY: 0,
      items: initialPictures,
    };
    this.loadingRef = React.createRef();
    this.currentIndex = this.currentIndex.bind(this);
    this.observing = (this.state.items < this.props.imgs.length);
  }

  componentDidMount() {
    this.startObserving(this.observing);
  }

  componentWillUnmount() {
    if (this.observing) {
      this.observer.unobserve(this.loadingRef.current);
    }
  }

  componentDidUpdate() {
    this.startObserving();
  }

  startObserving(force = false) {
	if ( force || (! this.observing) ) {
      this.observer= new IntersectionObserver(
        this.observed.bind(this),
        optionsObserver
      );
      this.observer.observe(this.loadingRef.current);
      this.observing = true;
    }
  }

  getTrueHeight(){
    let h = this.getElement()
    return h;
  }


  currentIndex() {
    return this.state.items;
  }

  observed(entries) {
    let e = entries[0];
    const y = e.boundingClientRect.y;
    this.setState({ items: Math.max(Math.min(this.props.imgs.length, this.state.items + loadPictureByPack), this.state.items) });
    this.setState({ lastY: y });
    if(this.props.imgs.length == this.state.items)
    { // end of the list
      this.observer.unobserve(this.loadingRef.current);
      this.observing = false;
    }
  }

  loadItems(start=0, end=this.props.imgs.length) {
	let i = start;
    return this.props.imgs.slice(start, end).map(
      (props) => (<div key={"pic-" + i++}>
                    <Image
                      {...props}
                      showTitle={this.props.config['image-title']}
                      showBorder={this.props.config['image-border']}
                    />
                  </div>)
    );
  }

  render() {
    let side = this.props.showSide < 0 ? 'hide-left' : ( this.props.showSide > 0 ? 'hide-right' : '');
    return (
      <div className={"contentHolder " + side} ref="ch">
        <div className="contentHolder-content">
          { this.loadItems(0, this.currentIndex() ) }
        </div>
        <div className="contentHolder-loading" ref={this.loadingRef}>{this.state.loading ? "Loading..." : ""}</div>
      </div>
    );
  }
}

