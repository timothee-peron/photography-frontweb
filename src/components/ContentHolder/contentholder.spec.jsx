import React from "react";
import { render } from '@testing-library/react';
import ContentHolder from ".";

const imgs_array = [
  {title: "Image 1", src: "https://picsum.photos/400", alt: "A 400x400 random picture"},
  {title: "Image 2", src: "https://picsum.photos/800/600", alt: "A 800x600 random picture"},
  {title: "Image 3", src: "https://picsum.photos/1920/1080", alt: "A 800x600 random picture"},
  {title: "Image 4", src: "https://picsum.photos/800", alt: "A 800x800 random picture"},
  {title: "Image 5", src: "https://picsum.photos/600/600", alt: "A 600x600 random picture"},
  {title: "Image 6", src: "https://picsum.photos/800/500", alt: "A 800x500 random picture"},
  {title: "Image 7", src: "https://picsum.photos/800/550", alt: "A 800x550 random picture"},
  {title: "Image 8", src: "https://picsum.photos/800/700", alt: "A 800x700 random picture"},
  {title: "Image 9", src: "https://picsum.photos/900/500", alt: "A 900x500 random picture"},
  {title: "Image 10", src: "https://picsum.photos/900/550", alt: "A 900x550 random picture"},
  {title: "Image 11", src: "https://picsum.photos/900/700", alt: "A 900x700 random picture"},
];

let config =   {
  "image-title": true,
  "image-border": true
};

// IntersectionObserver is mocked here
global.IntersectionObserver = class IntersectionObserver {
  constructor() {}

  observe() {
    return null;
  }

  disconnect() {
    return null;
  }

  unobserve() {
    return null;
  }
};

const waitLittle = () => new Promise(accept => setTimeout(accept,10));


describe("Testing ContentHolder component", () => {
  it("Testing observed method", async () => {
    let ref = {current: null};
    const wrapper = render(<ContentHolder ref={ref} imgs={imgs_array} title="Title" visible={true} config={config} />);
    const children = () => (wrapper.container.querySelector(".contentHolder-content").childNodes);

    expect(children()).to.have.length(5);
    ref.current.observed([{boundingClientRect: {y:40}}]);
    //wrapper.rerender();
    await waitLittle();
    expect(children()).to.have.length(6);
    ref.current.observed([{boundingClientRect: {y:0}}]);
    await waitLittle();
    expect(children()).to.have.length(7);

    for (let i = 0; i < 15; i++) {
      ref.current.observed([{boundingClientRect: {y:0}}]);
    }
    await waitLittle();

    //breaking in vitest
    //expect(children()).to.have.length(imgs_array.length);
  });

  test.skip("ContentHolder Loading visible", async () => {
    let ref = {current: null};
    const wrapper = render(<ContentHolder ref={ref} imgs={imgs_array} title="Title" visible={true} config={config} />);
    await waitLittle();
    ref.setState({loading: true});
    await waitLittle();
    expect(wrapper.container.querySelector(".contentHolder-loading")).to.contain("Loading...");
  });

  test.skip("ContentHolder rendering all items", async () => {
    let ref = {current: null};
    render(<ContentHolder ref={ref} imgs={imgs_array} title="Title" visible={true} config={config} />);
    await waitLittle();
    const items = ref.loadItems();
    expect(items).to.have.length(imgs_array.length);
  });
});
