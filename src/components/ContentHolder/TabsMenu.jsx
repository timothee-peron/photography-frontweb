import React, { Component } from "react";
import "./TabsMenu.scss";

class TabsMenu extends Component {
  constructor(props) {
    super(props);
  }

  onPrevious() {
    this.props.showPrevious();
  }

  onNext(){
    this.props.showNext();
  }

  render() {
    let tabs = this.props.tabs;
    let index = this.props.index;
    let length = tabs.length;

    if (length === 0){
      return ''
    }

    if (length === 1){
      return (
        <div className="tabs-menu">
          <h2 className="tabs-menu-current-tab">{tabs[index]}</h2>
        </div>
      )
    }

    let previous = ''
    if (index > 0)
    {
      previous = '<'
    }

    let next = ''
    if (index + 1 < length)
    {
      next = '>'
    }

    return (
      <div className="tabs-menu">
        <button disabled={(index > 0) ? false : true} className={"tabs-menu-arrow" + (index > 0 ? '' : " tabs-menu-arrow-hidden")} onClick={this.onPrevious.bind(this)}>
          {previous}
        </button>
        <h2 className="tabs-menu-current-tab">{tabs[index]}</h2>
        <button disabled={(index + 1 < length ? false : true)} className={"tabs-menu-arrow"+ (index + 1 < length ? '' : " tabs-menu-arrow-hidden")} onClick={this.onNext.bind(this)}>
          {next}
        </button>
      </div>
    )
  }
}

export default TabsMenu
