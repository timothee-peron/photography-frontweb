import React, { Component } from "react";
import "./LinkButton.scss";

export default class LinkButton extends Component {
  onClick(){
    this.props.onClick && this.props.onClick();
  }

  render() {
    return (
      <div className="linkButtonContainer">
        <button className="linkButtonBox" onClick={this.onClick.bind(this)}>
          {this.props.children}
        </button>
      </div>
    );
  }
}

