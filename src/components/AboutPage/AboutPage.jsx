import React, { Component } from "react";
import axios from "axios";
import LinkButton from "./LinkButton";
import "./AboutPage.scss";

class AboutPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      content: "",
    };
  }

  componentDidMount() {
    axios.get("/about.json").then(response => {
      this.setState({
        title: response.data.title,
        content: response.data.content
      });
    });
  }

  onClose() {
    this.props.onClose && this.props.onClose();
  }

  render() {
    return (
      <div className={ "aboutPage" + (this.props.visible ? "" : " hidden") }>
        <div className={ "aboutPage-box" + (this.props.visible ? "" : " hidden") }>
          {(this.props.visible) ? (
            <div className="aboutPage-content">
              <h1>{this.state.title}</h1>
              <p>{this.state.content}</p>
              <LinkButton onClick={this.onClose.bind(this)}>Close</LinkButton>
            </div>
          ) :"" }
        </div>
      </div>
    );
  }
}

export default AboutPage;

