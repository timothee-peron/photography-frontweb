import React, { Component } from "react";
import "./Header.scss";

class Header extends Component {
  clickAbout() {
    this.props.onAboutClick()
  }
  clickHome() {
    this.props.onHomeClick()
  }

  render() {
    let showAbout = this.props.config.about;
    return (
      <div className="header">
        <button onClick={this.clickHome.bind(this)}>Home</button>
        { showAbout ? <button onClick={this.clickAbout.bind(this)}>About</button> : '' }
      </div>
    );
  }
}

export default Header;
