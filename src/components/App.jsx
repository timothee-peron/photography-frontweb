import React from "react";
import Header from "./Header/Header";
import HomePage from "./HomePage/HomePage";
import AboutPage from "./AboutPage/AboutPage";
import "./App.scss";
import ConfigLoader from "./ConfigLoader/ConfigLoader";


class App extends React.Component {
  constructor(props) {
    super(props);

    this.config = new ConfigLoader();
    this.config.load().then(() => this.unmask());

    this.state = {
      masked: true,
      scrollable: this.webPageShowAbout(),
      showAbout: false,
    };
  }

  webPageShowAbout() {
    return this.state && this.state.showAbout;
  }

  updateScrollBody() {
    this.setState({scrollable: this.webPageShowAbout()});
  }

  unmask(){
    this.setState({masked: false});
  }

  onHomeClick() {
    this.setState({
      showAbout: false,
      scrollable: true,
    })
  }

  onAboutClick(){
    this.setState({
      showAbout: true,
      scrollable: false,
    })

  }

  render() {
    let showHeader = typeof this.config.getConfig() !== 'undefined' && this.config.getConfig().topbar;
    return (
      <div id="App" className={( this.webPageShowAbout() ? 'unscrollable' : 'scrollable ') + ' ' + (this.state.masked ? 'masked': 'unmasked')}>
        { showHeader ? <Header config={this.config.getConfig()} onHomeClick={this.onHomeClick.bind(this)} onAboutClick={this.onAboutClick.bind(this)} /> : '' }
        { !this.state.masked ? <AboutPage visible={this.webPageShowAbout()} onClose={this.onHomeClick.bind(this)}/> : '' }
        { !this.state.masked ? <HomePage config={this.config.getConfig()} blurred={this.state.showAbout} /> : '' }
      </div>
    );
  }
}

export default App;
