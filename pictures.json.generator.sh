#!/bin/bash
set -e;
#set -x;

#user for last commands, jenkins need to be a sudoer (e.g. user)
user=root

#dir name for work (e.g. dir1)
workDir=dir1
#path to where to fetch pictures
copyFrom=/home/$user/$workDir
# copy from structure:
# copyFrom/
#         /tab1_name/
#                   /picture1.jpg
#                   /picture2.jpg
#         /tab2_name/
#                   /picture1.jpg
#                   /picture2.jpg
#         /magick

#path to where to copy to pictures
copyToRoot=/home/root/dir2
#json generated name
jsonFileName=pictures.json
# where to copy json and pics to:
copyToJson=$copyToRoot/$jsonFileName
copyToPics=$copyToRoot/pics/

#install exiftool !!
#magick delivered as standalone, DL here: https://imagemagick.org/
magickPath=./$workDir

############################################### START

rm ./* -rf;

cp $copyFrom ./ -rv;
mv $magickPath/magick ./magick;
chmod u+x magick;


# json
json=$(jq --null-input \
          --arg title "Some Photos" \
'{"title": $title, "pictures": [], "defaultTab": "", "tabs": []}' \
);

# remove and replace metadata
me="me";
url="https://example.com/";

for dir in ./$workDir/*/
do

echo Dir is $dir;

#without backslashes:
tab=` echo $dir | sed "s/\.\/$workDir//" | sed 's/\///'  | sed 's/\/$//' `;
echo Tab is $tab

# add tab to json
json=$(echo ${json} | \
jq --arg tab ${tab} \
'.tabs[.tabs| length] |= . + $tab' \
);
defaultTab=$(echo ${json} | jq .defaultTab -r); # -r for raw output

if [ -z "$defaultTab" ] ; then
json=$(echo ${json} | \
jq --arg t ${defaultTab} \
'.defaultTab = $t' \
);
fi

_IFS=$IFS;
IFS=$'\n';
jpgs=$(ls -p $dir | grep -v / | grep -i '.jpg$');

for jpg in $jpgs
do
file=$dir$jpg;

echo File is $file;
exiftool -all= -overwrite_original ${file}
exiftool ${file} -overwrite_original \
  -creator="${me}" \
  -artist="${me}" \
  -by-line="${me}" \
  -copyright="${url}" \
  -copyrightNotice="${url}" \
  -rights="${url}";
./magick "${file}" -resize 800x800\> "${file}";

# add to json
json=$(echo ${json} | jq --arg jpg ${jpg} \
                       --arg tab ${tab} \
                       --arg path p/${tab}/${jpg} \
'.pictures[.pictures| length] |= . + { "title": $jpg, "src": $path, "alt": $jpg, "tab": $tab }' \
);

done
echo Exiting dir $dir;
done

IFS=$_IFS;

echo ${json} | jq '.' > ./$jsonFileName
sudo -u $user cp -f ./$jsonFileName $copyToJson
sudo -u $user rm -rf $copyToPics*
sudo -u $user cp -rf ./$workDir/* $copyToPics

rm ./magick
